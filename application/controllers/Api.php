<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('employees_model');
		$this->load->model('offices_model');
	}

	/**
	 * Retrieve organization structure
	 * @return JSON
	 */
	public function organization()
	{
		$db_select = 'employeeNumber, CONCAT(firstName, " ", lastName) AS name, jobTitle';
		$employee = $this->employees_model->getEmployee(['reportsTo' => null], $db_select);
		$employee->employeeUnder = $this->employees_model->getEmployeeUnder($employee->employeeNumber, $db_select);

		// Set header and print out data
		$this->output->set_content_type('application/json')->set_output(json_encode($employee));
	}

	public function offices() {
		$office_db_select = 'officeCode, city';
		$employee_db_select = 'employeeNumber, CONCAT(firstName, " ", lastName) as name, jobTitle';
		$offices = $this->offices_model->getOfficesWithEmployees(null, $office_db_select, $employee_db_select);

		// Set header and print out data
		$this->output->set_content_type('application/json')->set_output(json_encode($offices));
	}
}
