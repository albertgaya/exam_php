<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offices_model extends CI_Model {
	private $tbl = 'offices';

	public function getOffices($condition = null, $select = null) {
		if ($select) {
			$this->db->select($select);
		}

		return $this->db->get_where($this->tbl, $condition)->result();
	}

	public function getOfficesWithEmployees($condition = null, $selectOffices = null, $selectEmployees = null) {
		$offices = $this->getOffices($condition, $selectOffices);
		
		foreach ($offices as $office) {
			$office->employees = $this->employees_model->getEmployees(['officeCode' => $office->officeCode], $selectEmployees);
		}

		return $offices;
	}
}
