<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees_model extends CI_Model {
	private $tbl = 'employees';

	public function getEmployees($condition, $select = null) {
		if ($select) {
			$this->db->select($select);
		}

		return $this->db->get_where($this->tbl, $condition)->result();
	}

	public function getEmployee($condition, $select = null) {
		if ($select) {
			$this->db->select($select);
		}

		return $this->db->get_where($this->tbl, $condition)->row();
	}

	public function getEmployeeUnder($employeeNumber, $select = null) {
		if ($select) {
			$this->db->select($select);
		}

		$employees = $this->db->get_where($this->tbl, ['reportsTo' => $employeeNumber])->result();

		// Iterate each employees and retrieve each under employees
		foreach ($employees as $employee) {
			$this->db->where('reportsTo', $employee->employeeNumber);
			$this->db->from($this->tbl);
			
			$employee->employeeUnder = $this->db->count_all_results() ? $this->getEmployeeUnder($employee->employeeNumber, $select) : [];
		}

		return $employees;
	}
}
